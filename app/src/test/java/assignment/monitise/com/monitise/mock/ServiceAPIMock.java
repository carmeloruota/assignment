package assignment.monitise.com.monitise.mock;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import assignment.monitise.com.monitise.api.APICallback;
import assignment.monitise.com.monitise.api.IServiceAPI;
import assignment.monitise.com.monitise.model.Result;

/**
 * Created by Carmelo on 26/02/16.
 */
public class ServiceAPIMock implements IServiceAPI {

    private List<Result> mockList;

    public List<Result> getMockList() {
        return mockList;
    }

    @Override
    public void search(String terms, APICallback<Result> listener) {
        mockList = new ArrayList<>();
        mockList.add(new Result.Builder().artistName("Queen").trackName("Bohemian Rhapsody").artworkUrl100("http://...").build());
        mockList.add(new Result.Builder().artistName("Queen").trackName("We Will Rock You").artworkUrl100("http://...").build());
        listener.onSuccess(mockList);
    }
}
