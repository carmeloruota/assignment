package assignment.monitise.com.monitise;

import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import assignment.monitise.com.monitise.api.IServiceAPI;
import assignment.monitise.com.monitise.mock.ServiceAPIMock;
import assignment.monitise.com.monitise.search.SearchContract;
import assignment.monitise.com.monitise.search.SearchPresenter;
import assignment.monitise.com.monitise.util.NetworkHelper;

import static org.mockito.Mockito.verify;

/**
 * Created by Carmelo on 26/02/16.
 */

@RunWith(PowerMockRunner.class)
public class SearchPresenterTest {

    @Mock
    private SearchContract.View mRequestView;


    private SearchPresenter mRequestPresenter;
    private ServiceAPIMock serviceApiMock;


    @Before
    public void setupAddNotePresenter() {

        MockitoAnnotations.initMocks(this);
        serviceApiMock = new ServiceAPIMock();
        // Get a reference to the class under test
        mRequestPresenter = new SearchPresenter(mRequestView, serviceApiMock);
    }


    @PrepareForTest({ NetworkHelper.class, Log.class})
    @Test
    public void showResultsIfConnectionExists() {
        PowerMockito.mockStatic(NetworkHelper.class);
        PowerMockito.mockStatic(Log.class);
        PowerMockito.when(NetworkHelper.internetConnectionAvailable(mRequestView.getContext())).thenReturn(true);

       // When the presenter is asked to request data
        mRequestPresenter.requestData("FAKE DATA");

        verify(mRequestView).showDialog(); // shown in the UI
        verify(mRequestView).showResults(serviceApiMock.getMockList()); // show list
        verify(mRequestView).dismissDialog(); // dismiss in the UI
    }

    @PrepareForTest({ NetworkHelper.class})
    @Test
    public void showErrorIfConnectionNotExists() {
        PowerMockito.mockStatic(NetworkHelper.class);
        PowerMockito.when(NetworkHelper.internetConnectionAvailable(mRequestView.getContext())).thenReturn(false);

        // When the presenter is asked to request data
        mRequestPresenter.requestData("FAKE DATA");

        verify(mRequestView).showErrorMessage(); // shown in the UI
    }

}
