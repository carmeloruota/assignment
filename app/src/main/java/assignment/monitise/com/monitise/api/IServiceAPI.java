package assignment.monitise.com.monitise.api;

import assignment.monitise.com.monitise.model.Result;


/**
 * Created by Carmelo on 25/02/16.
 */
public interface IServiceAPI {
     void search(String terms, final APICallback<Result> listener);
}
