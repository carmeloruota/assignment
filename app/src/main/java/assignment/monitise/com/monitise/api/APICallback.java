package assignment.monitise.com.monitise.api;

import java.util.List;

/**
 * Created by Carmelo on 25/02/16.
 */
// this interface is used to communicate between APIClient and Presenters
public interface APICallback<T> {

    void onSuccess(List<T> T);
    void onError();
}
