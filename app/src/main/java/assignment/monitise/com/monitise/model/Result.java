package assignment.monitise.com.monitise.model;

import org.parceler.Parcel;

/**
 * Created by Carmelo on 25/02/16.
 */
@Parcel
public class Result {

    public Result() {
    }

    public String wrapperType;
    public String kind;
    public Integer artistId;
    public Integer collectionId;
    public Integer trackId;
    public String artistName;
    public String collectionName;
    public String trackName;
    public String collectionCensoredName;
    public String trackCensoredName;
    public String artistViewUrl;
    public String collectionViewUrl;
    public String trackViewUrl;
    public String previewUrl;
    public String artworkUrl30;
    public String artworkUrl60;
    public String artworkUrl100;
    public Double collectionPrice;
    public Double trackPrice;
    public String releaseDate;
    public String collectionExplicitness;
    public String trackExplicitness;
    public Integer discCount;
    public Integer discNumber;
    public Integer trackCount;
    public Integer trackNumber;
    public Integer trackTimeMillis;
    public String country;
    public String currency;
    public String primaryGenreName;
    public Boolean isStreamable;

    private Result(Builder builder) {
        wrapperType = builder.wrapperType;
        kind = builder.kind;
        artistId = builder.artistId;
        collectionId = builder.collectionId;
        trackId = builder.trackId;
        artistName = builder.artistName;
        collectionName = builder.collectionName;
        trackName = builder.trackName;
        collectionCensoredName = builder.collectionCensoredName;
        trackCensoredName = builder.trackCensoredName;
        artistViewUrl = builder.artistViewUrl;
        collectionViewUrl = builder.collectionViewUrl;
        trackViewUrl = builder.trackViewUrl;
        previewUrl = builder.previewUrl;
        artworkUrl30 = builder.artworkUrl30;
        artworkUrl60 = builder.artworkUrl60;
        artworkUrl100 = builder.artworkUrl100;
        collectionPrice = builder.collectionPrice;
        trackPrice = builder.trackPrice;
        releaseDate = builder.releaseDate;
        collectionExplicitness = builder.collectionExplicitness;
        trackExplicitness = builder.trackExplicitness;
        discCount = builder.discCount;
        discNumber = builder.discNumber;
        trackCount = builder.trackCount;
        trackNumber = builder.trackNumber;
        trackTimeMillis = builder.trackTimeMillis;
        country = builder.country;
        currency = builder.currency;
        primaryGenreName = builder.primaryGenreName;
        isStreamable = builder.isStreamable;
    }


    public static final class Builder {
        private String wrapperType;
        private String kind;
        private Integer artistId;
        private Integer collectionId;
        private Integer trackId;
        private String artistName;
        private String collectionName;
        private String trackName;
        private String collectionCensoredName;
        private String trackCensoredName;
        private String artistViewUrl;
        private String collectionViewUrl;
        private String trackViewUrl;
        private String previewUrl;
        private String artworkUrl30;
        private String artworkUrl60;
        private String artworkUrl100;
        private Double collectionPrice;
        private Double trackPrice;
        private String releaseDate;
        private String collectionExplicitness;
        private String trackExplicitness;
        private Integer discCount;
        private Integer discNumber;
        private Integer trackCount;
        private Integer trackNumber;
        private Integer trackTimeMillis;
        private String country;
        private String currency;
        private String primaryGenreName;
        private Boolean isStreamable;

        public Builder() {
        }

        public Builder wrapperType(String val) {
            wrapperType = val;
            return this;
        }

        public Builder kind(String val) {
            kind = val;
            return this;
        }

        public Builder artistId(Integer val) {
            artistId = val;
            return this;
        }

        public Builder collectionId(Integer val) {
            collectionId = val;
            return this;
        }

        public Builder trackId(Integer val) {
            trackId = val;
            return this;
        }

        public Builder artistName(String val) {
            artistName = val;
            return this;
        }

        public Builder collectionName(String val) {
            collectionName = val;
            return this;
        }

        public Builder trackName(String val) {
            trackName = val;
            return this;
        }

        public Builder collectionCensoredName(String val) {
            collectionCensoredName = val;
            return this;
        }

        public Builder trackCensoredName(String val) {
            trackCensoredName = val;
            return this;
        }

        public Builder artistViewUrl(String val) {
            artistViewUrl = val;
            return this;
        }

        public Builder collectionViewUrl(String val) {
            collectionViewUrl = val;
            return this;
        }

        public Builder trackViewUrl(String val) {
            trackViewUrl = val;
            return this;
        }

        public Builder previewUrl(String val) {
            previewUrl = val;
            return this;
        }

        public Builder artworkUrl30(String val) {
            artworkUrl30 = val;
            return this;
        }

        public Builder artworkUrl60(String val) {
            artworkUrl60 = val;
            return this;
        }

        public Builder artworkUrl100(String val) {
            artworkUrl100 = val;
            return this;
        }

        public Builder collectionPrice(Double val) {
            collectionPrice = val;
            return this;
        }

        public Builder trackPrice(Double val) {
            trackPrice = val;
            return this;
        }

        public Builder releaseDate(String val) {
            releaseDate = val;
            return this;
        }

        public Builder collectionExplicitness(String val) {
            collectionExplicitness = val;
            return this;
        }

        public Builder trackExplicitness(String val) {
            trackExplicitness = val;
            return this;
        }

        public Builder discCount(Integer val) {
            discCount = val;
            return this;
        }

        public Builder discNumber(Integer val) {
            discNumber = val;
            return this;
        }

        public Builder trackCount(Integer val) {
            trackCount = val;
            return this;
        }

        public Builder trackNumber(Integer val) {
            trackNumber = val;
            return this;
        }

        public Builder trackTimeMillis(Integer val) {
            trackTimeMillis = val;
            return this;
        }

        public Builder country(String val) {
            country = val;
            return this;
        }

        public Builder currency(String val) {
            currency = val;
            return this;
        }

        public Builder primaryGenreName(String val) {
            primaryGenreName = val;
            return this;
        }

        public Builder isStreamable(Boolean val) {
            isStreamable = val;
            return this;
        }

        public Result build() {
            return new Result(this);
        }
    }
}
