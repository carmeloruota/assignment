package assignment.monitise.com.monitise.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;

import org.parceler.Parcels;

import java.util.List;
import java.util.concurrent.TimeUnit;

import assignment.monitise.com.monitise.R;
import assignment.monitise.com.monitise.adapters.ResultsAdapter;
import assignment.monitise.com.monitise.api.ServiceAPI;
import assignment.monitise.com.monitise.detail.DetailActivity;
import assignment.monitise.com.monitise.model.Result;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

public class SearchActivity extends AppCompatActivity implements SearchContract.View, ResultsAdapter.OnItemSelectedListener {

    private SearchPresenter presenter;

    @Bind(R.id.recycleViewResults) RecyclerView recycleViewResults;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    private ResultsAdapter resultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        presenter = new SearchPresenter(this, new ServiceAPI());

        recycleViewResults.setLayoutManager(new LinearLayoutManager(this));
        recycleViewResults.hasFixedSize();

        resultsAdapter = new ResultsAdapter();
        resultsAdapter.setListener(this);

        recycleViewResults.setAdapter(resultsAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setIconified(true);
        RxSearchView.queryTextChanges(searchView)
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(new Observer<CharSequence>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(final CharSequence charSequence) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (charSequence.toString().length() > 2) {
                                    presenter.requestData(charSequence.toString().replaceAll(" {2,}", " ").replace(" ", "+"));
                                }
                                else
                                    showResults(null);
                            }
                        });
                    }
                });


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showDialog() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void dismissDialog() {
        progressBar.setVisibility(View.INVISIBLE);

    }

    @Override
    public void navigateToDetails(Result resultItem) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.INTENT_PARAM_ITEM, Parcels.wrap(resultItem));
        startActivity(intent);
    }

    @Override
    public void showResults(List<Result> resultsList) {
        resultsAdapter.setResultList(resultsList);
    }

    @Override
    public void showErrorMessage() {
        //TODO customize error message
        Toast.makeText(this, "An error has occurred", Toast.LENGTH_LONG).show();
        showResults(null);

    }

    /**
     * Function called when an item in the recycleview is tapped
     */
    @Override
    public void onItemSelected(int pos) {
        navigateToDetails(resultsAdapter.getItem(pos));
    }

}
