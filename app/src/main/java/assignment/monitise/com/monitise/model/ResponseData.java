package assignment.monitise.com.monitise.model;

import java.util.List;

/**
 * Created by Carmelo on 25/02/16.
 */
public class ResponseData {

    public int resultCount;
    public List<Result> results;

}
