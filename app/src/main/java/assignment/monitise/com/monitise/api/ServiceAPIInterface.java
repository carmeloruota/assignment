package assignment.monitise.com.monitise.api;

import assignment.monitise.com.monitise.model.ResponseData;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Carmelo on 25/02/16.
 */
public interface ServiceAPIInterface {

    @GET("search")
    Observable<ResponseData> search(@Query("term") String terms);

}
