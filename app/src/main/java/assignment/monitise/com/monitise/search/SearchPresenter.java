package assignment.monitise.com.monitise.search;

import android.util.Log;

import java.util.List;

import assignment.monitise.com.monitise.api.APICallback;
import assignment.monitise.com.monitise.api.IServiceAPI;
import assignment.monitise.com.monitise.model.Result;
import assignment.monitise.com.monitise.util.NetworkHelper;


/**
 * Created by Carmelo on 25/02/16.
 */
public class SearchPresenter implements SearchContract.Presenter {


    private static final String APP_TAG = "SEARCH_PRESENTER";
    private SearchContract.View view;
    private IServiceAPI serviceAPI;


    // we pass ServiceAPI instance so we can user a Mock in test environment
    public SearchPresenter(SearchContract.View view, IServiceAPI serviceAPI) {
        this.view = view;
        this.serviceAPI = serviceAPI;
    }


    // method called from view when user write text in the search bar
    @Override
    public void requestData(String searchTerms) {
        // test if internet connection is available
        if(!NetworkHelper.internetConnectionAvailable(view.getContext()))
        {
            view.showErrorMessage();
        }
        else
        {
            view.showDialog();

            // call search API
            serviceAPI.search(searchTerms, new APICallback<Result>() {

                // if http request success, the list of results is showed
                @Override
                public void onSuccess(List<Result> results) {
                    Log.d(APP_TAG,"Result count: "+results.size());
                    view.showResults(results);
                    view.dismissDialog();

                }

                // otherwise, a message to user is prompted
                @Override
                public void onError() {
                    view.dismissDialog();
                    view.showErrorMessage();
                }
            });
        }
    }
}
