package assignment.monitise.com.monitise.util;

/**
 * Created by Carmelo on 20/11/15.
 */
public class StringUtils {
    public static final String regexURL = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";


        /**
         * Function to convert milliseconds time to
         * Timer Format
         * Hours:Minutes:Seconds
         * */
        public static String milliSecondsToTimer(long milliseconds){
            String finalTimerString = "";
            String secondsString = "";
            String minutesString = "";

            // Convert total duration into time
            int hours = (int)( milliseconds / (1000*60*60));
            int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
            int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
            // Add hours if there
            if(hours > 0){
                finalTimerString = hours + ":";
            }

            // Prepending 0 to seconds if it is one digit
            if(seconds < 10){
                secondsString = "0" + seconds;
            }else{
                secondsString = "" + seconds;}

            if(minutes < 10){
                minutesString = "0" + minutes;
            }else{
                minutesString = "" + minutes;}


            finalTimerString = finalTimerString + minutesString + ":" + secondsString;

            // return timer string
            return finalTimerString;
        }


    public static boolean isUrl(String string) {
        return string != null && string.matches(regexURL);
    }
}
