package assignment.monitise.com.monitise.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Carmelo on 25/02/16.
 */
public class DateHelper {


    /**
     * Converts a date string format to another
     *
     * @param dateString original string representing date
     * @param  from format of original string date
     * @param to format of result string date
     * */
    public static String convertDataFormat(String dateString,String from,String to)
    {
        SimpleDateFormat simpleDateFormatFrom = new SimpleDateFormat(from, Locale.UK);
        SimpleDateFormat simpleDateFormatTo = new SimpleDateFormat(to, Locale.UK);

        try {
            Date date = simpleDateFormatFrom.parse(dateString);
            return simpleDateFormatTo.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // if error -> return the same date
        return dateString;

    }
}
