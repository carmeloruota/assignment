package assignment.monitise.com.monitise.api;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.List;

import assignment.monitise.com.monitise.model.ResponseData;
import assignment.monitise.com.monitise.model.Result;
import assignment.monitise.com.monitise.util.LoggingInterceptor;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Carmelo on 25/02/16.
 */
public class ServiceAPI implements IServiceAPI{

    public static String baseURL = "http://itunes.apple.com/";

    private final OkHttpClient client;
    private final ServiceAPIInterface serviceAPI;

    public ServiceAPI() {

        client = new OkHttpClient();
        client.interceptors().add(new LoggingInterceptor());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();


         serviceAPI = retrofit.create(ServiceAPIInterface.class);

    }


    public void search(String terms, final APICallback<Result> listener) {

        serviceAPI.search(terms)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Func1<ResponseData, List<Result>>() {
                    @Override
                    public List<Result> call(ResponseData responseData) {
                        //TODO check if something was wrong
                        return responseData.results;
                    }
                })
                .subscribe(new Subscriber<List<Result>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        listener.onError();
                    }

                    @Override
                    public void onNext(List<Result> results) {
                        listener.onSuccess(results);
                    }
                });

    }
}
