package assignment.monitise.com.monitise.search;

import android.content.Context;

import java.util.List;

import assignment.monitise.com.monitise.model.Result;

/**
 * Created by Carmelo on 25/02/16.
 */
public interface SearchContract {

    // methods used by Presenter to communicate with the View
    interface  View
    {
        Context getContext();
        void showDialog();
        void dismissDialog();
        void navigateToDetails(Result resultItem);
        void showResults(List<Result> resultsList);
        void showErrorMessage();
    }

    // actions called by View
    interface Presenter
    {
        void requestData(String serachTerms);
    }
}
