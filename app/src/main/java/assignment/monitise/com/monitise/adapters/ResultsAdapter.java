package assignment.monitise.com.monitise.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import assignment.monitise.com.monitise.R;
import assignment.monitise.com.monitise.model.Result;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Carmelo on 25/02/16.
 */
public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ViewHolder> {

    private List<Result> resultList = new ArrayList<>();
    private Context context;
    private OnItemSelectedListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        context = parent.getContext();

        // create a new view
        View itemLayoutView = LayoutInflater.from(context)
                .inflate(R.layout.item_result, parent, false);
        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Result tmp = resultList.get(holder.getAdapterPosition());

        holder.textArtist.setText(tmp.artistName);
        holder.textTitle.setText(tmp.trackName);
        Picasso.with(context).load(tmp.artworkUrl100).placeholder(R.mipmap.ic_launcher).into(holder.imageCover);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemSelected(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return resultList!=null?resultList.size():0;
    }

    public void setResultList(List<Result> resultList) {
        this.resultList = resultList;
        this.notifyDataSetChanged();
    }

    public Result getItem(int pos)
    {
        return  resultList.get(pos);
    }

    public void setListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnItemSelectedListener{
        void onItemSelected(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imageCover) ImageView imageCover;
        @Bind(R.id.textArtist) TextView textArtist;
        @Bind(R.id.textTitle) TextView textTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
