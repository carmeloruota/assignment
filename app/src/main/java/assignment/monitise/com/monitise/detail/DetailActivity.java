package assignment.monitise.com.monitise.detail;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.widget.IconTextView;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.Locale;

import assignment.monitise.com.monitise.R;
import assignment.monitise.com.monitise.model.Result;
import assignment.monitise.com.monitise.util.CircleTransform;
import assignment.monitise.com.monitise.util.DateHelper;
import assignment.monitise.com.monitise.util.StringUtils;
import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {

    public static final String INTENT_PARAM_ITEM = "paramItem";
    private Result result;


    @Bind(R.id.textAlbum) TextView textAlbum;
    @Bind(R.id.textArtist) TextView textArtist;
    @Bind(R.id.textTitle) TextView textTitle;
    @Bind(R.id.textDate) TextView textDate;
    @Bind(R.id.textPrice) TextView textPrice;
    @Bind(R.id.imageArtwork) ImageView imageArtwork;
    @Bind(R.id.textPlayer) IconTextView textPlayer;
    private MediaPlayer mediaPlayer;

    private Handler mpTimerHandler = new Handler();

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long currentPosition = getCurrentPosition();
            textPlayer.setText(StringUtils.milliSecondsToTimer(currentPosition));
            Log.d("PROGRESS","Time: "+currentPosition);
            mpTimerHandler.postDelayed(this, 100);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);


        this.result = Parcels.unwrap(getIntent().getExtras().getParcelable(INTENT_PARAM_ITEM));

        initLayout();
    }

    private void initLayout()
    {
        if(StringUtils.isUrl(result.previewUrl))
        {
            textPlayer.setVisibility(View.VISIBLE);
            initPlayer();
        }
        else
        {
            textPlayer.setVisibility(View.INVISIBLE);
        }

        textAlbum.setText(result.collectionName);
        textTitle.setText(result.trackName);
        textArtist.setText(result.artistName);
        textDate.setText(DateHelper.convertDataFormat(result.releaseDate,"yyyy-MM-dd'T'HH:mm:ss'Z'","yyyy-MM-dd"));
        textPrice.setText(String.format(Locale.UK,"Price %s %.2f",result.currency,result.trackPrice));

        Picasso.with(this).load(result.artworkUrl100).transform(new CircleTransform()).into(imageArtwork);
    }


    private void initPlayer()
    {
        textPlayer.setOnClickListener(this);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnPreparedListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case  android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        // if mediaplayer is playing the player is stopped
        if(mediaPlayer.isPlaying()) {
            mpTimerHandler.removeCallbacks(mUpdateTimeTask);
            mediaPlayer.stop();
            mediaPlayer.reset();
            textPlayer.setText("{fa-play}");
        }
        else {
            // else try to play the stream
            try {
                textPlayer.setText("{fa-spinner spin}");
                mediaPlayer.setDataSource(result.previewUrl);
                mediaPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
                textPlayer.setText("{fa-play}");
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // the player is started
        mediaPlayer.start();
        // used to update the song timer
        mpTimerHandler.postDelayed(mUpdateTimeTask, 100);
    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.stop();
        }
        mediaPlayer.reset();

        mpTimerHandler.removeCallbacks(mUpdateTimeTask);
   }

    public int getCurrentPosition()
    {
        return mediaPlayer.getCurrentPosition();
    }


    @Override
    protected void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.reset();
        super.onDestroy();
    }
}
