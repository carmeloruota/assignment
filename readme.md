# Application pattern used
This application was designed using the MVP patter.

There are two main packages:

- **search**: Contains the View and Presenter's implementations and interfaces for the 'Master' view

- **details**: The 'Details' view


As search box I used the SerachView object contained into toolbar

 The libraries used in order to consuming and process the REST API are: Retrofit, RxJava, Gson
 
 A UnitTest was written for the SearchPresenter component 
 
##Missing parts:##

 - I wanted to write an Espresso Test for testing the UI

 - The screen orientation is blocked to portrait mode 
 
###Extra:###
- A media player for listening the preview track
 
 
